use std::time::Duration;
use std::time::Instant;

const NANOS: [(char, u64); 7] = [
    ('n', 1),
    ('µ', 1000),
    ('m', 1000_000),
    (' ', 1000_000_000),
    ('k', 1000_000_000_000),
    ('M', 1000_000_000_000_000),
    ('G', 1000_000_000_000_000_000),
];

fn amount_text(nanos: u64) -> String {
    for &(suffix, ord) in NANOS.iter() {
        let rnd_offset = ord / 2;

        let scaled = (nanos + rnd_offset) / ord;
        if scaled < 10000 {
            return format!("{:4}{}", scaled, suffix);
        }
    }

    format!("XXXXXX")
}

fn duration_text(d: Duration) -> String {
    let nanos = 1000000000u64 * d.as_secs() as u64 + d.subsec_nanos() as u64;

    format!("{}s", amount_text(nanos))
}

fn main() {
    let start = Instant::now();
    let mut prev = start;
    let stdin = ::std::io::stdin();

    let mut line = String::new();

    loop {
        match stdin.read_line(&mut line) {
            Ok(0) => {
                /* EOF usually produces an empty file. */
                return;
            },
            Ok(_) => {
                let now = Instant::now();
                let durs = now.duration_since(start);
                let durp = now.duration_since(prev);

                print!("{} +{}: {}",
                       duration_text(durs),
                       duration_text(durp),
                       line);
                prev = now;
                line.clear();
            },
            Err(e) => {
                println!("received {}. Exiting", e);
                return;
            }
        }
    }
}

#[cfg(test)]
mod tests {
    /* First, some non-rounding cases */
    #[test]
    fn test_amount_text_1() {
        let text = ::amount_text(1);
        assert_eq!(&text, "   1n");
    }

    #[test]
    fn test_amount_text_1000() {
        let text = ::amount_text(1000);
        assert_eq!(&text, "1000n");
    }

    #[test]
    fn test_amount_text_9999() {
        let text = ::amount_text(9999);
        assert_eq!(&text, "9999n");
    }

    /* 10 µ is the first that will not fit */
    #[test]
    fn test_amount_text_10000() {
        let text = ::amount_text(10000);
        assert_eq!(&text, "  10µ");
    }

    /* This should still round into 10 µs */
    #[test]
    fn test_amount_text_10499() {
        let text = ::amount_text(10499);
        assert_eq!(&text, "  10µ");
    }

    #[test]
    fn test_amount_text_99999() {
        let text = ::amount_text(99_999);
        assert_eq!(&text, " 100µ");
    }

    #[test]
    fn test_amount_text_100u() {
        let text = ::amount_text(100_000);
        assert_eq!(&text, " 100µ");
    }

    #[test]
    fn test_amount_text_99999u() {
        let text = ::amount_text(99_999_000);
        assert_eq!(&text, " 100m");
    }

    #[test]
    fn test_amount_text_99999_499u() {
        let text = ::amount_text(99_999_499);
        assert_eq!(&text, " 100m");
    }

    #[test]
    fn test_amount_text_99999_500u() {
        let text = ::amount_text(99_999_500);
        assert_eq!(&text, " 100m");
    }
}
